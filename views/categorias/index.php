<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categorias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categorias-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Categorias', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php //  var_dump($dataProvider->models[1]->attributes['foto']); // sacar un dato en concreto ?> 
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            [
                    'attribute'=>'descripcion',
                    'content'=>function($data){
                        return mb_substr($data->descripcion,0,10);
                    }
            ]
            ,
            [
                         /* ayuda en : http://www.bsourcecode.com/yiiframework2/gridview-in-yiiframework-2-0/#gridview-column-add-image */
                    'label'=>'Foto',
                    'format'=>'raw',
                    'contentOptions'=>['style'=>'text-align:center'],
                    'value' => function($data){
                        return Html::a(
                                Html::img(
                                            "@web/imgs/categorias_imagenes/".$data->foto.".jpg",[
                                                'alt' => 'Imagen de categorias',
                                                'class'=>'foto1',
                                                ],
                                            ),
                                            [
                                                '/categorias/detalles','id'=>$data->id, // envia la id de la categoria como parametro get
                                            
                                            ],
                                ); 
                        }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>



    
    
</div>
