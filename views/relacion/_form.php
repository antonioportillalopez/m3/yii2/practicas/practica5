<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Relacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="relacion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'producto')->textInput() ?>

    <?= $form->field($model, 'categoria')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
