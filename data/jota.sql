﻿DROP DATABASE IF EXISTS jota;
CREATE DATABASE jota;

USE jota;

CREATE TABLE productos (
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(63),
  foto varchar(253),
  descripcion varchar(511),
  precio float,
  oferta tinyint
  );


CREATE TABLE relacion (
  id int AUTO_INCREMENT PRIMARY KEY,
  producto int,
  categoria int
  );

CREATE TABLE categorias (
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(253),
  descripcion varchar(511),
  foto varchar (253)
  );

ALTER TABLE relacion
ADD FOREIGN KEY (producto) REFERENCES productos(id); 

ALTER TABLE relacion
ADD FOREIGN KEY (categoria) REFERENCES categorias(id); 

INSERT INTO categorias(nombre, descripcion, foto) 
  VALUES 
  ('Smartphone','moviles nuevos y de segunda mano',1),
  ('Calculadoras','calculadoras cientificas',2),
  ('Relojes','Relojes de pulsera',3),
  ('Electronica', 'Componentes para realización de instalaciones electricas',4);

INSERT INTO productos (nombre,foto, descripcion, precio, oferta) 
  VALUES
  ('Boli',1,'marca Bic de toda la vida',140.41,FALSE),
  ('Casio f25',2,' Reloj nipones del penedes',230.63,TRUE),
  ('Gafas',3,'con cristales antireflectantes',457.19,FALSE),
  ('Folios',4,'Marca Galgo',741.25,FALSE),
  ('Cartera',5,'billetera,  monedero tarjetera de caballero ',158.02,TRUE),
  ('Bolso',6,'cuero rojo de mano',478.80,FALSE),
  ('Monitor',7,'Aos 25 pulgadas tft con altavoces y hdmi',741,FALSE),
  ('Soldador',8,'JVC el de siempre',1122.25,TRUE),
  ('Carpeta',9,'con separadores y de carton',52,FALSE),
  ('Calendario',10,' tipo bolsillo',45.1,FALSE);

INSERT INTO relacion (producto, categoria)
  VALUES
  (2,3),
  (1,3),
  (3,4),
  (5,3),
  (9,1),
  (7,4),
  (6,1),
  (4,4),
  (8,1),
  (10,1),
  (3,2),
  (5,1),
  (9,2),
  (10,2);


SELECT * FROM categorias c;
SELECT * FROM productos p;
SELECT * FROM relacion r;