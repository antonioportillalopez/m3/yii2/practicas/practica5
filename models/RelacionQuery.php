<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Relacion]].
 *
 * @see Relacion
 */
class RelacionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Relacion[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Relacion|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
