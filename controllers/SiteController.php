<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\ContactoForm;
use app\models\Productos;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
       return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function contact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * PRUEBA DE ENVIO DE CORREO ELECTRONICO
    public function actionCorreo()
    {
        $correo=new ContactForm();
        $correo->asunto='Probando este rollo';
        $correo->contenido='El contenido del correo';
        $correo->correo='antport@gmail.com';
        $correo->nombre='Cliente';
        $correo->contact(Yii::$app->params["informacion"]);
    }
     */
    
    public function actionInformacion() {
        $m=new ContactForm();
        if($m->load(
                Yii::$app->request->post())
                && 
                $m->contact(Yii::$app->params["informacion"])
                ){
                    Yii::$app->session->setFlash('enviadaInformacion');
                    return $this->refresh();
                    
        }
            
            return $this->render("informacion",[
                "model"=>$m,
              ]);
    }
    
    public function actionContacto() {
        $modelo=new ContactoForm();
        
        //$model->load(Yii::$app->request->post())
        if($modelo->load(
                Yii::$app->request->post())
                && 
                $modelo->cont(Yii::$app->params["contacto"])
                ){
                    Yii::$app->session->setFlash('enviadaInformacion');
                    return $this->refresh();
                    
        }
            
            return $this->render("contacto",[
                "model"=>$modelo,
              ]);
        
                
        
    }
    
    public function actionProductos(){
        //$query="select * from productos";
        $activeQuery=Productos::find();
        
        $dataProvider=new ActiveDataProvider([
            "query"=>$activeQuery,
        ]);
        
        return $this->render("productos_view",[
            "data"=>$dataProvider,
        ]);
        
    }

    
}
