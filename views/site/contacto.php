<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>



<?php
if(Yii::$app->session->hasFlash("enviadaInformacion")){
    echo '<div class="alert alert-success">Solicitud enviada</div>';
}
?>



  <div class="row">
            <div class="col-lg-5">
                
                <?php
                /*
                 * ActiveForm es una clase pero que no se instancia
                 * Cuando creo un objeto de tipo activeForm lo realizo mediante begin
                 * $form es un objeto de tipo ActiveForm
                 * field es un metodo de ActiveForm que genera un objeto de tipo ActiveField
                 * $form->field() es un objeto de tipo ActiveField
                 * Los objetos de tipo ActiveField tienen metodos 
                 *  - textInput
                 *  - dropDownList
                 * 
                 * $form->field()->dropdown
                 */
                
                ?>

                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                    <?= $form->field($model, 'nombre')->textInput(['autofocus' => true]) ?>
                
                    <?= $form->field($model, 'apellidos') ?>

                    <?= $form->field($model, 'correo') ?>
                    <!-- metodo dropDownList sale del procedimiento begin() de la clase ActiveForm  que genera
                    un objeto de clase form que tiene el procedimiento dropDownlist -->
                    <?= $form->field($model, 'tema')->dropDownList ($model->tema)//https://www.yiiframework.com/doc/api/2.0/yii-widgets-activefield#dropDownList()-detail ?>

                    <?= $form->field($model, 'asuntos')->textarea(['rows' => 6]) ?>

                    

                    <div class="form-group">
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
