
<?php 
use yii\widgets\DetailView;
use yii\helpers\Html;
use yii\grid\GridView;


echo DetailView::widget([
    'model' => $modelo,
    'attributes' => [
        'id',
        'nombre',
        'descripcion',
        'foto',
        [ 
            'label'=>'foto',
            'format'=>'raw',
            'value' =>Html::img("@web/imgs/categorias_imagenes/".$modelo->foto.".jpg",[
                                                'alt' => 'Imagen de categorias',
                                                'class'=>'foto1']) 
        ],
    ],
]);





?>
<h4> Productos de esa categoría</h4>
 <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'nombre',
            'precio',
            'descripcion',
            
            [
             
                    'label'=>'Foto',
                    'format'=>'raw',
                    'contentOptions'=>['style'=>'text-align:center'],
                    'value' => function($data){
                        return Html::img(
                                            "@web/imgs/productos_imagenes/".$data->foto.".jpg",[
                                                'alt' => 'Imagen de categorias',
                                                'class'=>'foto2',
                                                ],
                                         
                                        ); 
                        }
            ],

            
        ],
    ]); ?>

