<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactoForm extends Model
{
    public $nombre;
    public $apellidos;
    public $correo;
    public $tema=[
                    'general', 
                    'productos',
                    'reclamaciones',
                    ];
    public $asuntos;
    
    


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['nombre', 'correo', 'tema', 'asuntos'], 'required'],
            // email has to be a valid email address
            ['correo', 'email'],
            // verifyCode needs to be entered correctly
            ['asuntos','string', 'length'=>[5,50]],
            [['tema','apellidos'],'safe'],
            
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Introduce tu nombre',
            'apellidos'=>'Introduce tus apellidos',
            'correo' => 'Introduce dirección tu email',
            'tema' => 'Introduce tu tema',
            'asuntos' => 'Introduce tus asuntos a tratar',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function cont($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setReplyTo([$this->correo => $this->nombre.' '.$this->apellidos])
                ->setSubject($this->tema)
                ->setTextBody($this->asuntos.'Lo envia: '.$this->nombre.' '.$this->apellidos)
                ->send();

            return true;
        }
        return false;
    }
}
