<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'gestionesinformaticasportilla@gmail.com',
    'senderName' => 'Antonio Portilla',
    'informacion'=> 'gestionesinformaticasportilla@gmail.com',
    'contacto'=> 'antport@gmail.com',
];
